﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(hotelmanagement.Startup))]
namespace hotelmanagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
